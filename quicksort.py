
def partitionArray(A, l_p, r_p):
    if r_p - l_p <= 1: return
    pivot_index = l_p
    pivot = A[pivot_index]
    i =  l_p + 1
    for j in range(i, r_p):
        if A[j] < pivot:
            A[i], A[j] = A[j], A[i]
            i += 1
    A[pivot_index], A[i-1] = A[i-1], pivot
    partitionArray(A, l_p, i - 1)
    partitionArray(A, i, r_p)

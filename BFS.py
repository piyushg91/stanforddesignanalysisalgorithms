import matplotlib.pyplot as plt
import networkx as nw
from random import randint
from Queue import Queue

#Performs BFS on subsection of graph
def sub_BFS(graph, queue, visited):
    while queue.qsize() > 0:
        working_node = queue.get()
        for node in graph.neighbors(working_node):
            if node not in visited:
                queue.put(node)
                visited.append(node)

def BFS(graph, to_draw = False):
    visited = []
    queue = Queue()
    for i in nodes:
        if i not in visited:
            queue.put(i)
            visited.append(i)
            sub_BFS(graph, queue, visited)
    assert set(visited) == set(nodes)
    if to_draw:
        nw.draw_networkx(graph)
    return visited

if __name__ == "__main__":
    nodes = range(randint(4,6))
    graph = nw.fast_gnp_random_graph(len(nodes), .25, directed=True)
    print(graph.edges())
    print(BFS(graph, to_draw = True))
    plt.show()

import matplotlib.pyplot as plt
import networkx as nw
from random import randint

#Performs DFS on subsection of graph
def sub_DFS(graph, stack, visited):
    while stack:
        working_node = stack.pop()
        if working_node not in visited:
            visited.append(working_node)
            for node in graph.neighbors(working_node):
                stack.append(node)

def DFS(graph, to_draw = False):
    visited = []
    stack = []
    for i in nodes:
        if i not in visited:
            stack.append(i)
            sub_DFS(graph, stack, visited)
    assert set(visited) == set(nodes)
    if to_draw:
        nw.draw_networkx(graph)
    return visited

def generate_random_DAG(nodes, edges):
    G = nw.DiGraph()
    for i in range(nodes):
        G.add_node(i)
    while edges > 0:
        a = randint(0,nodes-1)
        b=a
        while b==a:
            b = randint(0,nodes-1)
        G.add_edge(a,b)
        if nw.is_directed_acyclic_graph(G):
            edges -= 1
        else:
            G.remove_edge(a,b)
    return G

def topological_sort(nodes, to_draw):
    graph = generate_random_DAG(len(nodes), len(nodes) + 4);
    order = []
    stack = []
    print graph.edges()
    for i in nodes:
        if i not in order:
            stack.append(i)
            sub_topological_sort(graph, stack, order)
    return order

def sub_topological_sort(graph, stack, order):
    explored = set()
    for node in nodes:
        if node in explored:
            continue
        stack.append(node)
        while stack:
            last = stack[-1]
            if last in explored:
                stack.pop()
                continue
            new_nodes = []
            for neighbor in graph.neighbors(last):
                if neighbor not in explored:
                    new_nodes.append(neighbor)
            if new_nodes:
                stack.extend(new_nodes)
            else:
                n = stack.pop()
                explored.add(n)
                order.append(n)


if __name__ == "__main__":
    nodes = range(100)
    order = topological_sort(nodes, to_draw=True)
    assert set(order) == set(nodes)
   
if False:
    graph = nw.fast_gnp_random_graph(len(nodes), .50, directed=True)
    print(graph.edges())
    print(DFS(graph, to_draw = True))
    plt.show()

count = 0

class Test():	
	def merge_sort(self, li):
		if len(li) < 2: return li
		m = len(li) / 2 
		return self.merge(self.merge_sort(li[:m]), self.merge_sort(li[m:])) 

	def merge(self,l, r):
		global count
		result = [] 
		i = j = 0 
		while i < len(l) and j < len(r): 
			if l[i] < r[j]: 
				result.append(l[i])
				i += 1 
			else: 
				result.append(r[j])
				count = count + (len(l) - i)
				j += 1
		result.extend(l[i:]) 
		result.extend(r[j:]) 
		return result

integers =[]
for line in open('IntegerArray.txt'): 
    line = line.rstrip('\n')
    integers.append(int(line))
    
def count_inv(inp):
	n = len(inp)
	if n == 1:
		return (inp, 0)
	left = inp[0:n/2]
	right = inp[n/2:]
	(a,x) = count_splitinv_and_merge(*count_inv(left))
	(b,y) = count_splitinv_and_merge(*count_inv(right))
	(c,z) = count_splitinv_and_merge(a+b, x+y)
	return c, z

def count_splitinv_and_merge(inp, invs):
	total_length = len(inp)
	a, b = inp[0:total_length/2], inp[total_length/2:]
	l1, l2 = len(a) , len(b)
	output = [None] * total_length
	i, j = 0, 0
	inversions = invs
	for elem in range(total_length):
		if i < l1 and j < l2:
			if a[i] < b[j]:
				output[elem] = a[i]
				i += 1
			else:
				output[elem] = b[j]
				j += 1
				inversions += l1 - i
		else:
			if j == l2:
				output[elem] = a[i]
				i += 1
			else:
				output[elem] = b[j]
				j += 1
	return output, inversions

def run_test(inp):
	a = Test()
	a.merge_sort(inp)
	my_prog = count_inv(inp)
	assert count == my_prog[1] , "count is %s, we got %s" % (count, my_prog[1])
	print "test passed"
	print my_prog[1]

run_test("5 5 5 5 5 5 5".split(" "))

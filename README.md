Python scripts for basic algorithms. This is really just for internal purposes. When I learn algorithms, I implement them via python and keep them on a git source for reference.

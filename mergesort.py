
def combine_and_sort(a, b):
	l1, l2 = len(a) , len(a)
	total_length = l1 + l2
	output = [None] * total_length
	i, j = 0, 0
	for elem in range(total_length):
		if i < l1 and j < l2:
			if a[i] < b[j]:
				output[elem] = a[i]
				i += 1
			else:
				output[elem] = b[j]
				j += 1
		else:
			if j == l2:
				output[elem] = a[i]
				i += 1
			else:
				output[elem] = b[j]
				j += 1			
	return output
	

def split_and_sort(arry):
	l = len(arry)
	if l == 1:
		return arry
	left_half = arry[0:l/2]
	right_half = arry[l/2:]
	return combine_and_sort(split_and_sort(left_half), split_and_sort(right_half))


if __name__ == "__main__":
    integers = [int(line.rstrip("\n")) for line in open('IntegerArray.txt')]
    print split_and_sort(integers)
